import React from 'react'

import Content from '../components/Content'

const Register = () => (
  <div>
    Register
    <Content page="Register Page" />
  </div>
)

export default Register
