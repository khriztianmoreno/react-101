import React from 'react'

import Talk from '../components/Talk'

class Detail extends React.PureComponent {
  state = {
    user: {
      name: {
        first: '',
      }
    },
    talk: {},
  }

  componentDidMount() {
    this.getSingleTalk()
  }

  getSingleTalk = async () => {
    const { match: { params } } = this.props
    try {
      const response = await fetch(`http://localhost:8080/talks/${params.id}`)
      const data = await response.json()
      if (data) {
        this.getUser(data.userId)
        this.setState({ talk: data })
      }
    } catch (error) {
      console.error(error)
    }
  }

  getUser = async (id) => {
    try {
      const response = await fetch(`http://localhost:8080/users/${id}`)
      const data = await response.json()
      if (data) {
        this.setState({ user: data })
      }
    } catch (error) {
      console.error(error)
    }
  }

  render() {
    console.log(this.state)
    const { user: { name }, talk } = this.state
    return <Talk {...talk} name={name} />
  }
}

export default Detail
