import React from 'react'

import Talk from '../components/Talk'

class Home extends React.PureComponent {
  state = {
    talks: []
  }

  componentDidMount() {
    fetch('http://localhost:8080/talks')
      .then(response => response.json())
      .then(data => this.setState({talks: data}))
      .catch(error => {
        console.error(error)
      })
  }

  render() {
    const { talks } = this.state
    return (
      <div>
        Home Page from Component
        {
          talks.map((talk, idx) => <Talk {...talk} key={idx} />)
        }
      </div>
    )
  }
}
export default Home
