import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'

import Home from './pages/Home';
import Register from './pages/Register';
import Detail from './pages/Detail';

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" component={Home} exact />
        <Route path="/register" component={Register} exact />
        <Route path="/talks/:id" component={Detail} exact />
      </Switch>
    </BrowserRouter>
  )
}

export default App
