import React from 'react'

import Navbar from './Navbar'

const Content = ({page}) => (
  <div>
    <h1>{page}</h1>
    <Navbar />
  </div>
)

export default Content
