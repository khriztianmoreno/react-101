import React from 'react'
import { Link } from 'react-router-dom'

const Talk = ({ title, about, picture, id }) => {
  const styles = {
    width: '200px',
    fontFamily: 'Arial',
  }
  return (
    <div>
      <h2>
        <Link to={`/talks/${id}`}>{title}</Link>
      </h2>
      <p>{about}</p>
      <img style={styles} src={picture} alt="jholi" />
    </div>
  )
}

export default Talk
