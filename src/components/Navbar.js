import React from 'react'
import { Link } from 'react-router-dom'

const Navbar = () => (
  <ul>
    <li>
      {/* <a href="/">Home</a> */}
      <Link to="/">Home</Link>
    </li>
    <li>
      <Link to="/register">Register</Link>
    </li>
    <li>
      <Link to="/talks/123">Detail</Link>
    </li>
  </ul>
)

export default Navbar
